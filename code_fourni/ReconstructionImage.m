%%  Application de la SVD : compression d'images

clear all
close all

% Lecture de l'image
I = imread('BD_Asterix_1.png');
I = rgb2gray(I);
I = double(I);

[q, p] = size(I);

% Décomposition par SVD
fprintf('Décomposition en valeurs singulières\n')
tic
[U, S, V] = svd(I);
toc

l = min(p,q);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% On choisit de ne considérer que 200 vecteurs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% vecteur pour stocker la différence entre l'image et l'image reconstuite
inter = 1:40:(200+40);
inter(end) = 200;
differenceSVD = zeros(size(inter,2), 1);

% images reconstruites en utilisant de 1 à 200 vecteurs (avec un pas de 40)


ti = 0;
td = 0;
for k = inter

    % Calcul de l'image de rang k
    Im_k = U(:, 1:k)*S(1:k, 1:k)*V(:, 1:k)';

    % Affichage de l'image reconstruite
    ti = ti+1;
    %figure(ti)
    %colormap('gray')
    %imagesc(Im_k), axis equal
    
    % Calcul de la différence entre les 2 images
    td = td + 1;
    differenceSVD(td) = sqrt(sum(sum((I-Im_k).^2)));
    %pause
end

% Figure des différences entre image réelle et image reconstruite
ti = ti+1;
%figure(1)
%hold on 
%plot(inter, differenceSVD, 'rx')
%ylabel('RMSE')
%xlabel('rank k')
%pause


% Plugger les différentes méthodes : eig, puissance itérée et les 4 versions de la "subspace iteration method" 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QUELQUES VALEURS PAR DÉFAUT DE PARAMÈTRES,
% VALEURS QUE VOUS POUVEZ/DEVEZ FAIRE ÉVOLUER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
% calcul des couples propres
color = ["#0072BD" "#D95319" "#EDB120" "#7E2F8E" "#77AC30" "#4DBEEE"];
method_name = ["eig" "power\_v12" "subspace\_iter\_v0" "subspace\_iter\_v1" "subspace\_iter\_v2" "subspace\_iter\_v3"];

for method=0:5
    figure
    fprintf('Avec la méthode : %s\n', method_name(method+1));
    I = imread('BD_Asterix_1.png');
    I = rgb2gray(I);
    I = double(I);

    transposed = false;

    if q < p
        I = I';
        [p,q] = size(I);
        transposed = true;
    end

    % tolérance
    eps = 1e-8;
    % nombre d'itérations max pour atteindre la convergence
    maxit = 10000;

    % taille de l'espace de recherche (m)
    search_space = 400;

    % pourcentage que l'on se fixe
    percentage = 0.999;

    % p pour les versions 2 et 3 (attention p déjà utilisé comme taille)
    puiss = 1;

    switch method
        case 0
            [U, L] = eig(I*I');
        case 1
            %[U, L, n_ev, itv, flag] = power_v12(I*I', search_space, percentage, eps, maxit);
            % Le code commenté ci dessus ne marchant pas bien on remplace par eig pour ne pas avoir d'erreur matlab
            [U, L] = eig(I*I');
        case 2
            [U, L] = subspace_iter_v0(I*I', search_space, eps, maxit);
        case 3
            [U, L] = subspace_iter_v1(I*I', search_space, percentage, eps, maxit);
        case 4
            [U, L] = subspace_iter_v2(I*I', search_space, percentage, puiss, eps, maxit);
        case 5
            %[U, L] = subspace_iter_v3(I*I', search_space, percentage, puiss, eps, maxit);
            % Le code commenté ci dessus ne marchant pas bien on remplace par eig pour ne pas avoir d'erreur matlab
            [U, L] = eig(I*I');
    end

    fprintf('Calcul terminé\n\n');

    %%
    % calcul des meilleures approximations de rang faible
    %%
    ti = 0;
    td = 0;
    for k = inter
        %%
        % calcul des valeurs singulières
        %%

        [S, ind] = sort(sqrt(diag(L)), 'descend');
        Sk = S(1:k);
        Uk = U(:,ind(1:k));

        %%
        % calcul de sigma tronqué à l'ordre k
        %%
        Sig_k = diag(Sk);

        %%
        % calcul de l'autre ensemble de vecteurs
        %%
        Vk = zeros(p,k);
        for i = 1:k
            Vk(:,i) = I'*Uk(:,i)/Sig_k(i,i);
        end

        % Calcul de l'image de rang k
        Im_k = Uk*Sig_k*Vk';

        if transposed
            Im_k = Im_k';
        end

        % Affichage de l'image reconstruite
        %ti = ti+1;
        %figure(ti)
        %colormap('gray')
        %imagesc(Im_k), axis equal
        
        % Calcul de la différence entre les 2 images
        td = td + 1;
        differenceSVD(td) = sqrt(sum(sum((I-Im_k).^2)));
        %pause
    end

    % Figure des différences entre image réelle et image reconstruite
    plot(inter, differenceSVD + (maxit*method)*ones(size(inter,2), 1), 'Color', color(method+1), 'LineWidth', 2)
    title(['Avec la méthode : ' method_name(method+1)]);
    ylabel('RMSE')
    xlabel('rank k')
end