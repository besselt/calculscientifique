%Nombre de colonne des matrices
n = 400;

D1 = zeros(n,1);
D2 = zeros(n,1);
D3 = zeros(n,1);
D4 = zeros(n,1);

%%% MAT 1
cond1 = n;
        
for i = 1:n
    D1(i) = n-i+1;
end

%%% MAT 2
cond2 = 1e10;
alpha2 = log(1/cond2);
        
for i = 1:n
    D2(i) = exp(alpha2*rand(1));
end

%%% MAT 3
cond3 = 1e5;
        alpha3 = cond3^(-1/(n-1));
        
        for i = 1:n
            D3(i) = alpha3^(i-1);
        end

%%% MAT 4
cond4 = 1e2;
        temp = 1/cond4;
        alpha4 = (1-temp)/(n-1);
        
        for i = 1:n
            D4(i) = (n-i)*alpha4 + temp;
        end

%tri des vp dans l'ordre décroissant = on affiche les vp domminante en
%premier
D1 = sort(D1, 'descend');
D2 = sort(D2, 'descend');
D3 = sort(D3, 'descend');
D4 = sort(D4, 'descend');

%%% Création des courbes de représentation des VP
x = 1:n;
figure
tiledlayout("flow");

nexttile
semilogy(x, D1, x, D2, x, D3, x, D4)
legend("imat1", "imat2", "imat3", "imat4")
title("Distribution des valeurs propres de la matrice généré en échelle semilog y (taille n=400)")
xlabel("Valeurs propres")
